import java.util.Arrays;

/**
 * 
 */

/**
 * @author EALARCON
 *
 */
public class ArrayTestApp 
{

	/**
	 * @param args
	 */
	public static void main(String[] args) 
	{
		/*
		double[] prices;
		prices = new double[4];
		*/

		//double[] prices = new double[4];
		/*
		//Java unlike MATLAB uses zero-based indexing
		prices[0] = 14.95;
		prices[1] = 12.95;
		prices[2] = 11.95;
		prices[3] = 9.95;
		*/
		//Don't attempt to access array elements greater 
		//than the length of the array.
		//Statements will compile but will follow under the
		//ArrayIndexOutOfBoundsException Error
		
		double[] prices = { 14.95, 12.95, 11.95, 9.95 };
		
		//To get the length of an array, use arrayName.length
		System.out.println( "Length of the prices array: " + prices.length );
		
		int[] values = new int[ 10 ];
		
		System.out.println( Arrays.toString(values) );		
		
		for( int i = 0; i < values.length; i++ )
		{
			values[i] = i;
		} //end for loop
		
		System.out.println( Arrays.toString(values) );
		
		String[] strings = new String[3];
		
		System.out.println( Arrays.toString( strings ) );
		
		String[] moreStrings = { "Hello", "Goodbye", "Hello Again" };
		
		System.out.println( Arrays.toString( moreStrings ) );
		
		for( int i = 0; i < moreStrings.length; i++ )
		{
			System.out.print( "\"" + moreStrings[i] + "\"" );
			if( i < moreStrings.length - 1 )
			{
				System.out.print( " , " );
			} //end if-statement
			
		} //end for loop
		
		System.out.println();
		
		//declare an accumulator variable
		double sum = 0.0;
		
		for( int i= 0; i < prices.length; i++ )
		{
			sum += prices[i];
		} //end for loop
		
		double average = sum / prices.length;
		
		System.out.println( "Average of prices: $" + average );
				
		double sum2 = 0.0;
		
		//Enhanced for loop
		for( double price : prices ) //for each prices in the price array do magic
		{
			sum2 = sum2 + price;
		}
		
		double average2 = sum2 / prices.length;
		
		System.out.println( "Average of prices: $" + average2 );
		
		/* swap demo */
		int[] myArray = new int[ 2 ];
		myArray[ 0 ] = 1;
		myArray[ 1 ] = 2;
		System.out.println( "Before swapping in main: " + Arrays.toString( myArray ) );
		
		//The swap is done here in the main method
		int temp = myArray[ 0 ];
		myArray[ 0 ]  = myArray[ 1 ];
		myArray[ 1 ] = temp;
		
		System.out.println( "After swapping in main: " + Arrays.toString( myArray ) );

		//We are still passing by VALUE, but the value passed
		//are copies of the elements stored in myArray[ 0 ]
		//and myArray[ 1 ]
		swapUsingElements( myArray[ 0 ], myArray[ 1 ] );
		
		System.out.println( "After passing ELEMENTS to swap in method:: " + Arrays.toString( myArray ) );		
		
		//We are still passing by VALUE, but the value passed
		//is a copy of the REFERENCE to the array.
		swapUsingRefToArray( myArray );
		
		System.out.println( "After passing REFERENCE TO ARRAY to swap in method:: " + Arrays.toString( myArray ) );		

		/* 
		 * start of ICE for 3 Feb 2017 
		 * Copy and paste all of this text at the bottom of the 
		 * main method in ArrayTestApp.java (before the closing brace!)
		 */
		
		// print a blank line
		System.out.println();
		
		// What do you think this statement does?
		double[] pricesCopyAttempt = prices; //creates new ref to prices
		
		// watch what happens:
		pricesCopyAttempt[0] = 11.11;
		System.out.println( 
				"pricesCopyAttempt: " + Arrays.toString( pricesCopyAttempt ) );
		System.out.println( "prices: " + Arrays.toString( prices ) );
		
		// do both pricesCopyAttempt and prices point to the same array?
		boolean testResult;
		testResult = ( pricesCopyAttempt == prices );
		System.out.println( 
		   "pricesCopyAttempt & prices refer to the SAME array: " + testResult);
		
		// do they have the same data?
		testResult = Arrays.equals( pricesCopyAttempt, prices );
		System.out.println( 
		   "pricesCopyAttempt & prices contain the same DATA: " + testResult);
		
		// If you really want to make a COPY of the prices array, use:
		double[] pricesCopy = Arrays.copyOf( prices, prices.length);
		
		testResult = ( pricesCopy == prices );
		System.out.println( 
		   "pricesCopy & prices refer to the SAME array: " + testResult);
		
		// do they have the same data?
		testResult = Arrays.equals( pricesCopy, prices );
		System.out.println( 
		   "pricesCopy & prices contain the same DATA: " + testResult);
		
		// Now, look what happens when we update one of the elements
		// in the actual COPY of the prices array:
		pricesCopy[0] = 22.22;
		System.out.println( 
				"pricesCopy: " + Arrays.toString( pricesCopy ) );
		System.out.println( "prices: " + Arrays.toString( prices ) );
		
		// do they have the same data now? (Obviously not, but...)
		testResult = ( pricesCopy == prices );;
		System.out.println( 
		   "pricesCopy & prices contain the same DATA: " + testResult);
		
		// Blank line for readability...
		System.out.println();
		
		// attempt to use binarySearch to find the index
		// position of 12.95 in the prices array:
		int searchKey = Arrays.binarySearch( prices, 9.95 );
		System.out.println( 
		   "Without sorting first, the returned search key is " + searchKey );
		
		// In order to use binarySearch, the search space must be SORTED first!
		Arrays.sort( prices );

		System.out.println( "prices, sorted: " + Arrays.toString( prices ) );
		
		searchKey = Arrays.binarySearch( prices, 999.95 );
		System.out.println( 
		   "But AFTER sorting, the returned search key is " + searchKey );
		
		// Blank line for readability...
		System.out.println();
		
		try 
		{
			prices[4] = 6.66;
		} 
		
		catch (ArrayIndexOutOfBoundsException e) 
		{
			
			//e.printStackTrace();
			//Print out "custom" error message
			System.err.println( "You Attempted to Access Prices[" + e.getMessage() + "]" );
		}
		
		//prices[4] = 7.77;
		System.out.println( "Program will terminate now..." );
		
	} //end method main

	public static void swapUsingElements( int x, int y )
	{
		System.out.println( "Before swap, x = " + x + ", y = " + y );
		int temp = x;
		x = y;
		y = temp;
		System.out.println( "After swap, x = " + x + ", y = " + y );
	} //end method swapUsingElements
	
	public static void swapUsingRefToArray( int[] refToArray )
	{
		System.out.println( "Before swap, refToArray[ 0 ] = " + refToArray[ 0 ] + ", refToArray[ 1 ] = " + refToArray[ 1 ] );		
		int temp = refToArray[ 0 ];
		refToArray[ 0 ] = refToArray[ 1 ];
		refToArray[ 1 ] = temp;
		System.out.println( "After swap, refToArray[ 0 ] = " + refToArray[ 0 ] + ", refToArray[ 1 ] = " + refToArray[ 1 ] );
	} //end method swapUsingRefToArray

} //end class ArrayTestApp
